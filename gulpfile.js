var gulp = require("gulp");
var pug = require("gulp-pug");
var data = require("gulp-data");
var sass = require("gulp-sass");
var cleanCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var path = require('path');

var scriptSources = [
    './js/lib/jquery.js',
    './js/lib/isotope.js',
    './js/lib/bootstrap.js',
    './js/lib/vivus.js',
    './js/lib/tweenjs.js',
    './js/lib/easeljs.js',
    './js/lib/owl.carousel.js',
    './js/lib/jquery.knob.min.js'
];

var cssSources = [
    './css/bootstrap.css',
    './css/owl.carousel.css',
    './css/owl.theme.css',
    './css/et-icons.css'
];

gulp.task('pug', function(){
    return gulp.src("./pug/*.pug")
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest("./dist/"));
});

gulp.task('sass', function(done) {
    return gulp.src('./sass/*.scss')
        .pipe(sass())
        .pipe(cleanCss())
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('scripts', function(done) {
    return gulp.src('./js/*.js')
    .pipe(concat('main.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('lib-css', function(done) {
    return gulp.src('./sass/lib/*.css')
    .pipe(concat('lib.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('./dist/css/'));
});

gulp.task('lib-js', function(done) {
    return gulp.src(scriptSources)
    .pipe(concat('lib.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});


gulp.task('lib', ['lib-js', 'lib-css']);

gulp.task('watch', function(){
    gulp.watch(['./pug/*.pug', './pug/**/*.pug', './sass/*.scss', './sass/**/*.scss', './js/*.js'], ['pug', 'sass', 'scripts']);
});