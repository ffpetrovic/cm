var stage;
var displayCircles = true;
var stickyNav = $('nav.navigation.sticky');
var scrollPrev = 0;
var clientsSlider;
var testimonialsSlider;

var introEl, pulseEl, footerEl;

var myVivus;

var postIntroTop;
var clientScrolled = false;

var time = 5, $elem, isPause, tick, percentTime;

var services = [];

function animHide(el, anim) {
	$(el).addClass('animated').css({
		"-webkit-animation-name": anim,
		"-moz-animation-name": anim,
		"-ms-animation-name": anim,
		"-o-animation-name": anim,
		"animation-name": anim
	}).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		$(el).css({
			'display': '',
			"-webkit-animation-name": '',
			"-moz-animation-name": '',
			"-ms-animation-name": '',
			"-o-animation-name": '',
			"animation-name": ''
		}).removeClass('animated').unbind("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend");
	});
}

function animShow(el) {
	$(el).addClass('animated');
	$(el).show();
}

// :: load
if($('body').hasClass('home')) {
	$(document).ready(function(){
	    $(this).scrollTop(0);
	    stage = new createjs.Stage("canvas-pulse");
		createjs.Ticker.setFPS(60);
		stage.canvas.width = $('body').innerWidth();
		stage.canvas.height = window.innerHeight;
	});

	$(window).on('load', function(event) {
		createjs.Ticker.addEventListener("tick", tick);
		stage.canvas.height = window.innerHeight;
		stage.update();

		introEl = $('.intro');
		footerEl = $('footer');
		pulseEl = $('#canvas-pulse');

		$('body').css('padding-top', window.innerHeight + 'px');
		$('body').css('padding-bottom', $('footer').outerHeight() + 'px')
		animShow($('.title-holder'));

		footerEl.addClass('hidden');

		$('.service-icon').each(function(index, el) {
			$(this).attr('id', 'service-icon-' + index);
			services.push( myVivus = new Vivus('service-icon-' + index, {
				type: 'async',
			    duration: 40,
			    start: 'manual',
			    animTimingFunction: Vivus.EASEOUT
			}));
		}); // Initializing Service Icon Animations

		var projectItemMinWidth = $('.project-grid .project-item').eq(0).outerWidth(true);
		$('.project-grid .project-item').each(function(index, el) {
			if($(this).width() < projectItemMinWidth)
				projectItemMinWidth = $(this).outerWidth();
		}); // Get the smallest column width, because, otherwise, masonry will use the width of the first element, in this case the largest one.

		$('.project-grid').masonry({
	  		itemSelector: '.project-item',
	  		columnWidth: projectItemMinWidth
		});

		clientsSlider = $('.clients-slider').owlCarousel({
			items: 4,
			pagination: false,
			itemsDesktop: [1024, 3],
			itemsTablet: [768, 2]
		}).data('owl-carousel');

		testimonialsSlider = $('.testimonials-slider').owlCarousel({
			singleItem : true,
			afterInit : progressBar,
			afterMove : moved,
			startDragging : pauseOnDragging,
			pagination: false,
			mouseDrag: false,
			touchDrag: false,
			addClassActive : true
		}).data('owl-carousel');

		$('.testimonials-slider').on({
		    mouseenter: function () {
		        isPause = true;
		    },
		    mouseleave: function () {
		        isPause = false;
		    }
		}); // Pausing Test. Slider On Hover

		$('.clients-slider .owl-item').eq(0).addClass('active'); // Make the first client active

		var index = $('.testimonials-slider .owl-item.active').index();
		var count = $('.testimonials-slider .owl-item').length;
		$('.slider-counter .slider-current').html(index + 1); 
		$('.slider-counter .slider-length').html(count); 

		postIntroTop = $('.post-intro').offset().top;

		$('body').css('opacity', '1');

		new Vivus('brain-svg', {
			type: 'delayed',
	    	duration: 70,
	    	animTimingFunction: Vivus.EASEOUT
	    }, initCircle);

	    $(".dial").knob({
		    'min':0,
		    'max':100,
		    'displayInput': false,
		    'thickness': .2,
		    'width': 15,
		    'height': 15,
		    'bgColor': 'transparent',
		    'fgColor': '#0c6ff6',
		    'readOnly': true
		});
	   $('.dial').parent().addClass('dial-holder');
	});


	$('.client').on('click', function(event) {
		event.preventDefault();
		$('.clients-slider .owl-item').removeClass('active');
		$(this).parent().addClass('active');
		testimonialsSlider.goTo($(this).parents().index());
	}); // Switching testimonials on client click

	$('.service').hover(function() { // Triger forwards animation on hover
		services[$(this).index()].stop().play();
		var svgCur = $(this).find(".service-icon")[0];
		var subdoc = getSubDocument(svgCur);
		var paths = subdoc.getElementsByClassName('st0');
		for (var i = 0; i < paths.length; i++) {
			 paths[i].style.stroke = "#0c6ff6";
		}
	}, function() { // Triger backwards animation on hover
		services[$(this).index()].stop().play(-1);
		var svgCur = $(this).find(".service-icon")[0];
		var subdoc = getSubDocument(svgCur);
		var paths = subdoc.getElementsByClassName('st0');
		for (var i = 0; i < paths.length; i++) {
			 paths[i].style.stroke = "#bbbbbb";
		}
	});


	$('.scroll-alert a').on('click', function(event) {
		event.preventDefault();
		scrollToPost();
	}); // Scroll to intro section on mouse icon click

	$('.project-view-more').on('click', function(event) {
		event.preventDefault();
		$('.project-overlay').addClass('active');
		animShow($('.project-description').show());
		$('.scroll-alert').fadeOut();
		$(this).fadeOut();
	}); // Open project overlay

	$('nav.navigation.fixed-top .menu-toggle').on('click', function(event) {
		event.preventDefault();
		if($(this).hasClass('active')) { // Menu is open
			animHide($('.menu-overlay'), 'fadeOut');
			$(this).removeClass('active');
			enableScroll();
		} else {						 // Menu is closed
			animShow($('.menu-overlay'));
			$(this).addClass('active');
			disableScroll();
		}
	}); // Open intro menu

	

	// Animation Functions
	



	function initCircle() {
		$('.intro .animated').each(function(index, el) {
			if($(this).hasClass('heading')) {
				$(this).css('display', 'inline-block');
			} else {
				$(this).css('display', 'block');
			}
		});
		$('nav.navigation .animated').each(function(index, el) {
			if($(this).hasClass('menu-toggle') || $(this).hasClass('nav-logo')) {
				$(this).css('display', 'inline-block');
			} else {
				$(this).css('display', 'block');
			}
		});
		addCircle();
		setInterval(function(){
			addCircle();
		}, 1000);
	} // Initalizing Intro Animation

	function addCircle() {
		var circle = new createjs.Shape();
		circle.graphics.beginStroke("#ddd").drawCircle(0, 0, 50);
		circle.graphics.setStrokeStyle(1, null, null, null, true);
		circle.x = stage.canvas.width / 2;
		circle.y = stage.canvas.height / 2;
		circle.alpha = 0;
		createjs.Tween.get(circle).to({alpha:1, scaleX: 8, scaleY:8}, 2000).to({alpha:0, scaleX: 16, scaleY:16, visible:false}, 2000).call(function() {
				stage.removeChild(circle);
			});
		
		stage.update();
		stage.addChild(circle);
	}


	function handleComplete(child) {
	    
	}

	function tick(event) {
		stage.update(event);
	}








	// -------------------------------------------------------------
	// Intro - Post Intro Scrolling functionality

	var allowed = false;
	var state = 'intro';
	var checkingScroll = false;
	var animating = false;

	$('body').on('click', '.intro', function(event) {
		event.preventDefault();
		if($(event.target).parents(".project-overlay").length == 0) {
			event.preventDefault();
			$('.project-overlay').removeClass('active');
			animHide($('.project-description'), 'fadeOut');
			animHide($('.close-overlay'), 'fadeOut');
			$('.scroll-alert').fadeIn();
			$('.project-view-more').fadeIn();
		}
	}); // Closing Project Overlay

	// :: scroll
	$(window).on('scroll', function(event) {
		event.preventDefault();
		if($(window).scrollTop() <= window.innerHeight) {
			if(!animating) {
				if(state == 'post') {
					if(!allowed) {
						$(document).scrollTop(postIntroTop);
						if(!checkingScroll) {
							disableScroll();
							checkingScroll = true;
							setTimeout(function(){
								allowed = $(window).scrollTop() <= window.innerHeight;
								checkingScroll = false;
								enableScroll();
							}, 500);
						}
					} else {
						$(document).scrollTop(postIntroTop);
						scrollToIntro();
						$('nav.navigation.sticky').removeClass('active');
						$('nav.navigation.sticky .menu-toggle').removeClass('active');
						$('.sidebar').removeClass('active');
						setTimeout(function(){
							$('.menu-overlay').removeClass('sidebar');
						}, 1000);
					}
				}
				if(state == 'intro') {
					scrollToPost();
				}
			}
			if(pulseEl.hasClass('hidden')) {
				pulseEl.removeClass('hidden');
				footerEl.addClass('hidden');
			}
		} else {
			if(!pulseEl.hasClass('hidden')) {
				pulseEl.addClass('hidden');
				footerEl.removeClass('hidden');
			}
		}
	});

	function scrollToIntro() {
		animating = true;
		disableScroll();
		$('.intro').removeClass('active');
		stickyNav.removeClass('active');
		$('html, body').animate({
			scrollTop: 0
		}, 1000, function() {
			allowed = false;
			state = 'intro';
			animating = false;
			enableScroll();
		});
	}
	function scrollToPost() {
		animating = true;
		disableScroll();
		$('.intro').addClass('active');
		$('html, body').animate({
			scrollTop: $(".post-intro").offset().top + 1
		}, 1000, function() {
			state = 'post';
			animating = false;
			allowed = false;
			enableScroll();
			$('nav.navigation.sticky').addClass('active');
			$('.menu-overlay').addClass('sidebar');
		});
	}

	function preventDefault(e) {
	  e = e || window.event;
	  if (e.preventDefault)
	      e.preventDefault();
	  e.returnValue = false;  
	}
	function preventDefaultForScrollKeys(e) {
	    if (keys[e.keyCode]) {
	        preventDefault(e);
	        return false;
	    }
	}
	function disableScroll() {
	  if (window.addEventListener) // older FF
	      window.addEventListener('DOMMouseScroll', preventDefault, false);
	  window.onwheel = preventDefault; // modern standard
	  window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
	  window.ontouchmove  = preventDefault; // mobile
	  document.onkeydown  = preventDefaultForScrollKeys;
	  console.log('disable');
	}
	function enableScroll() {
	    if (window.removeEventListener)
	        window.removeEventListener('DOMMouseScroll', preventDefault, false);
	    window.onmousewheel = document.onmousewheel = null; 
	    window.onwheel = null; 
	    window.ontouchmove = null;  
	    document.onkeydown = null;  
	  console.log('enable');

	}


	$(window).on('scroll', startClientSlider);
	function startClientSlider() {
		if($(window).scrollTop() + $(window).height() >= $('.clients-slider').offset().top) {
			start();
			clientScrolled = true;
			$(window).unbind('scroll', startClientSlider);
		}
	}


	function progressBar(elem){
		$elem = elem;
	}

	function start() {
		percentTime = 0;
		isPause = false;
		tick = setInterval(interval, 10);
	};

	function interval() {
		if(isPause === false){
			percentTime += 1 / time;
			$('.dial').val(percentTime).trigger('change');
			if(percentTime >= 100){
				$elem.trigger('owl.next');
				clientsSlider.goTo($('.clients-slider .owl-item.active').index());
			}
		}
	}

	function pauseOnDragging(){
		isPause = true;
	}

	function moved(){
		if(clientScrolled) {
			$('.clients-slider .owl-item').removeClass('active');
			var index = $('.testimonials-slider .owl-item.active').index();
			var count = $('.testimonials-slider .owl-item').length;
			$('.clients-slider .owl-item').eq(index).addClass('active');
			$('.slider-counter .slider-current').html(index + 1); 
			$('.slider-counter .slider-length').html(count); 
			clearTimeout(tick);
			start();
		}
	}
}

function getSubDocument(embedding_element) {
	if (embedding_element.contentDocument) 
	{
		return embedding_element.contentDocument;
	} 
	else 
	{
		var subdoc = null;
		try {
			subdoc = embedding_element.getSVGDocument();
		} catch(e) {}
		return subdoc;
	}
}

$('nav.navigation.sticky .menu-toggle').on('click', function(event) {
	event.preventDefault();
	if($(this).hasClass('active')) { // Menu is open
		// animHide($('.menu-overlay'), 'menuFadeOutRight');
		$(this).removeClass('active');
		$('.menu-overlay').removeClass('active');
	} else {						 // Menu is closed
		// animShow($('.menu-overlay'));
		$(this).addClass('active');
		$('.menu-overlay').addClass('active');
	}
}); // Open sidebar menu

$('.menu-overlay .nav-center ul li').each(function(index, el) {
	var interval = 75;
	var offset = 100;	
	$(this).css({
		"-webkit-transition-delay": (index * interval + offset) + 'ms',
		"-moz-transition-delay":    (index * interval + offset) + 'ms',
		"-ms-transition-delay":     (index * interval + offset) + 'ms',
		"-o-transition-delay":      (index * interval + offset) + 'ms',
		"transition-delay":         (index * interval + offset) + 'ms'
	});
}); // Applying incrementing transition delays for sidebar menu items

//uncomment this to make pause on mouseover 
// ----------------------------------------
function invertNav(invert) {
	if(invert) {
		$('nav.navigation.sticky').addClass('inverted');
		$.each(getSubDocument($('nav.navigation.sticky .nav-logo .logo-symbol')[0]).getElementsByClassName('st0'), function(index, value) {
			value.setAttribute('class', 'st0 active');
		});
		$('nav.navigation.sticky .menu-toggle').addClass('inverted');
	} else {
		$('nav.navigation.sticky').removeClass('inverted');
		$.each(getSubDocument($('nav.navigation.sticky .nav-logo .logo-symbol')[0]).getElementsByClassName('st0'), function(index, value) {
			value.setAttribute('class', 'st0');
		});
		$('nav.navigation.sticky .menu-toggle').removeClass('inverted');
	}
}

function initInvert() {
	$('nav.navigation.sticky .nav-logo .logo-symbol')[0].addEventListener('load', function() {
		invertNav(true);
	});
}

function browserWidth(width) {
	return $(window).width() <= width;
}

if($('body').hasClass('page')) {
	$(window).on('load', function(event) {
		$('body').css('opacity', '1');
		$('nav.navigation.sticky .menu-toggle, nav.navigation.sticky .nav-logo').css('display', 'inline-block');
		
		if($('.navigation-invert').length) {
			initInvert();
			$(window).on('scroll', invertNavScroll);
		}

		if(browserWidth(991)) {
			$('.level-holder.level-holder-one').addClass('active');
		}
	});

	$('[data-menu]').on('click', function(event) {
		event.preventDefault();
		$(this).parents('ul').find('[data-menu]').parent().removeClass('active');
		$(this).parent().addClass('active');
		$(this).parents('.level-holder').next().find('ul').removeClass('active');
		$($(this).attr('data-menu')).addClass('active');
		$(this).parents('.level-holder').removeClass('active');
		$(this).parents('.level-holder').next().addClass('active');
	});

	$('.level-menu-back').on('click', function(event) {
		event.preventDefault();
		$(this).parents('.level-holder').prev().addClass('active');
		$(this).parents('.level-holder').removeClass('active');
	});

	$('.career-scroll').on('click', function(event) {
		event.preventDefault();
		$('html, body').animate({
			scrollTop: $('#careers').offset().top - $('nav.navigation.sticky').outerHeight()
		}, 1000);
	});

	function invertNavScroll() {
		if($(window).scrollTop() < $('.navigation-invert').offset().top - $('nav.navigation.sticky').height() && !$('nav.navigation.sticky').hasClass('inverted')) {
			invertNav(true);
		} else if($(window).scrollTop() >= $('.navigation-invert').offset().top - $('nav.navigation.sticky').height() && $('nav.navigation.sticky').hasClass('inverted')) {
			invertNav(false);
		}
	}

	$('.v-height-eq').parent().each(function(index, el) {
		var maxHeight = 0;
		$(this).find('.v-height-eq').each(function(index, el) {
			if($(this).height() > maxHeight)
				maxHeight = $(this).height();
			console.log($(this));
		});
		$(this).find('.v-height-eq').height(maxHeight);
	});
}